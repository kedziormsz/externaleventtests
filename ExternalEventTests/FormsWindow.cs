﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExternalEventTests
{
    public partial class FormsWindow : Form
    {
        ViewModel vm;

        public FormsWindow(ViewModel vm)
        {
            this.vm = vm;

            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Hide();
            vm.pickDoor();
            Show();

            label1.Text = vm.Status;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            vm.FlipFacingExEvent.Raise();

            label1.Text = vm.Status;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            vm.FlipHandExEvent.Raise();

            label1.Text = vm.Status;
        }
    }
}
