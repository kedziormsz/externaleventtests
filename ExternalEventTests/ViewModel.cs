﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using SzleksToolkit.Revit.Tools;

namespace ExternalEventTests
{
    public class ViewModel : MyINotifyPropertyChangedTemplate
    {
        ExternalCommandData commandData;
        Action doEvents;

        public ViewModel(ExternalCommandData commandData, Action doEvents)
        {
            this.commandData = commandData;
            this.doEvents = doEvents;
        }

        internal ViewModel initExEvents()
        {
            bool dummy = FlipFacingExEvent.IsPending;

            return this;
        }

        #region window binded props

        private bool isVisible = true;
        public bool IsVisible
        {
            get { return isVisible; }
            set
            {
                isVisible = value;
                NotifyPropertyChanged("IsVisible");
            }
        }

        private string status = "start tests";
        public string Status
        {
            get { return status; }
            set
            {
                status = value;
                NotifyPropertyChanged("Status");
            }
        }

        #endregion // window binded props

        internal void setStatus(object x)
        {
            string s = x.ToString();
            Status = s;
            Debug.WriteLine(s);
            doEvents();
        }

        #region commands

        internal IList<Element> doors;

        internal void pickDoor()
        {
            IsVisible = false;

            //lock (doors) ???
            {
                try
                {
                    doors = Picking.pickSeveral(
                        commandData.Application.ActiveUIDocument,
                        BuiltInCategory.OST_Doors).ToList();
                }
                catch (Autodesk.Revit.Exceptions.OperationCanceledException) { }
            }
            Status = doors != null ? doors.Count.ToString() : "null";

            IsVisible = true;
        }

        private ICommand pick;
        public ICommand Pick
        {
            get
            {
                return pick ?? (pick = new RelayCommand(
                    (x) =>
                    {
                        pickDoor();
                    },
                    (x) => { return IsVisible; }));
            }
        }

        private ICommand clear;
        public ICommand Clear
        {
            get
            {
                return clear ?? (clear = new RelayCommand(
                    (x) =>
                    {
                        doors = null;
                        Status = "null";
                    }));
            }
        }

        private ExternalEvent flipFacingExEvent;
        public ExternalEvent FlipFacingExEvent
        {
            get { return flipFacingExEvent ?? (flipFacingExEvent = ExternalEvent.Create(new FlipFacingHandler(this))); }
        }

        private ICommand flipFacing;
        public ICommand FlipFacing
        {
            get
            {
                return flipFacing ?? (flipFacing = new RelayCommand(
                    (x) =>
                    {
                        FlipFacingExEvent.Raise();
                    },
                    (x) =>
                    {
                        return !FlipFacingExEvent.IsPending;
                    }));
            }
        }

        private ExternalEvent flipHandExEvent;
        public ExternalEvent FlipHandExEvent
        {
            get
            {
                return flipHandExEvent ?? (flipHandExEvent = ExternalEvent.Create(
                    new FlipHandHandler(this, x => setStatus(x))));
            }
        }

        private ICommand flipHand;
        public ICommand FlipHand
        {
            get
            {
                return flipHand ?? (flipHand = new RelayCommand(
                    (x) =>
                    {
                        FlipHandExEvent.Raise();
                    },
                    (x) =>
                    {
                        return !FlipHandExEvent.IsPending;
                    }));
            }
        }

        private ICommand statusTest;
        public ICommand StatusTest
        {
            get
            {
                return statusTest ?? (statusTest = new RelayCommand(
                    (x) =>
                    {
                        Task.Factory.StartNew(() =>
                        {
                            for (int i = 0; i < 100; i++)
                            {
                                Status = i.ToString();
                                Thread.Sleep(20);
                            }
                        });
                    },
                    (x) =>
                    {
                        return true;
                    }));
            }
        }

        #endregion // commands
    }
}
