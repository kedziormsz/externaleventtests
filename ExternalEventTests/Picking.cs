﻿// (c) maciek szlęk, kedziormsz@gmail.com, http://maciejszlek.pl/

using System.Collections.Generic;
using System.Collections.ObjectModel;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;

namespace SzleksToolkit.Revit.Tools
{
    class MyElementFilter : ISelectionFilter
    {
        private BuiltInCategory bic;

        public MyElementFilter(BuiltInCategory c)
        {
            bic = c;
        }

        public bool AllowElement(Element e)
        {
            return e.Category.Id.IntegerValue.Equals((int)bic);
        }

        public bool AllowReference(Reference r, XYZ p)
        {
            return false;
        }
    }

    class MyElementMultiFilter : ISelectionFilter
    {
        private BuiltInCategory[] bic;

        public MyElementMultiFilter(BuiltInCategory[] c)
        {
            bic = c;
        }

        public bool AllowElement(Element e)
        {
            foreach (BuiltInCategory c in bic)
                if (e.Category.Id.IntegerValue.Equals((int)c)) return true;

            return false;
        }

        public bool AllowReference(Reference r, XYZ p)
        {
            return false;
        }
    }

    public static class Picking
    {
        #region helpers
        static Element getElementFromReference(Reference r, UIDocument uidoc)
        {
#if RVT12
            return uidoc.Document.GetElement(r);
#else
            return r.Element;
#endif
        }

        static ICollection<Element> getElementsFromReferences(IList<Reference> rs, UIDocument uidoc)
        {
            var elems = new Collection<Element>();

            foreach (Reference r in rs)
                elems.Add(getElementFromReference(r, uidoc));

            return elems;
        }
        #endregion helpers

        public static Element pickOne(
          UIDocument uidoc,
          string message = "Please select an element")
        {
            Reference r = uidoc.Selection
                .PickObject(ObjectType.Element, message);

            return r != null ? getElementFromReference(r, uidoc) : null;
        }

        public static Element pickOne(
          UIDocument uidoc,
          BuiltInCategory c,
          string message = "Please select an element")
        {
            Reference r = uidoc.Selection.PickObject(
                ObjectType.Element,
                new MyElementFilter(c),
                message);

            return r != null ? getElementFromReference(r, uidoc) : null;
        }

        public static Element pickOne(
          UIDocument uidoc,
          BuiltInCategory[] c,
          string message = "Please select an element")
        {
            Reference r = uidoc.Selection.PickObject(
                ObjectType.Element,
                new MyElementMultiFilter(c),
                message);

            return r != null ? getElementFromReference(r, uidoc) : null;
        }

        public static ICollection<Element> pickSeveral(
          UIDocument uidoc,
          string message = "Please select elements")
        {
            IList<Reference> rs =
                uidoc.Selection.PickObjects(ObjectType.Element, message);

            return getElementsFromReferences(rs, uidoc);
        }

        public static ICollection<Element> pickSeveral(
          UIDocument uidoc,
          BuiltInCategory c,
          string message = "Please select elements")
        {
            IList<Reference> rs = uidoc.Selection.PickObjects(
                ObjectType.Element,
                new MyElementFilter(c),
                message);

            return getElementsFromReferences(rs, uidoc);
        }

        public static ICollection<Element> pickSeveral(
          UIDocument uidoc,
          BuiltInCategory[] c,
          string message = "Please select elements")
        {
            IList<Reference> rs = uidoc.Selection.PickObjects(
                ObjectType.Element,
                new MyElementMultiFilter(c),
                message);

            return getElementsFromReferences(rs, uidoc);
        }
    }
}
