﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ExternalEventTests
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class WpfWindow : Window
    {
        public WpfWindow()
        {
            InitializeComponent();
        }

        private void Pick_Click(object sender, RoutedEventArgs e)
        {
            (DataContext as ViewModel).pickDoor();
        }

        private void FlipFacing_Click(object sender, RoutedEventArgs e)
        {
            (DataContext as ViewModel).FlipFacingExEvent.Raise();
        }

        private void FlipHand_Click(object sender, RoutedEventArgs e)
        {
            // rvt crash

            //Application.Current.Dispatcher.Invoke(
            //    DispatcherPriority.Background,
            //    new ThreadStart(delegate { }));

            (DataContext as ViewModel).FlipHandExEvent.Raise();

            //Dispatcher.BeginInvoke(new Action(() =>
            //{
            //    (DataContext as ViewModel).FlipHandExEvent.Raise();
            //}));
        }

        private void StatusTest_Click(object sender, RoutedEventArgs e)
        {
            //var vm = (DataContext as ViewModel);

            // rvt crash

            //Application.Current.Dispatcher.Invoke(
            //    DispatcherPriority.Background,
            //    new ThreadStart(delegate { }));

            //for (int i = 0; i < 100; i++)
            //{
            //    vm.Status = i.ToString();
            //    Thread.Sleep(20);
            //}
        }

        // http://stackoverflow.com/a/11899439

        public void DoEvents()
        {
            DispatcherFrame frame = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
                new DispatcherOperationCallback(ExitFrame), frame);
            Dispatcher.PushFrame(frame);
        }

        public object ExitFrame(object f)
        {
            ((DispatcherFrame)f).Continue = false;

            return null;
        }
    }
}
