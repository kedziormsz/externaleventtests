﻿///:
///@: maciej szlęk
/// kedziormsz@gmail.com
/// http://maciejszlek.pl/
///

using System;
using System.Windows.Data;
using System.Globalization;
using System.Windows;

namespace ExternalEventTests
{
    /// <summary>
    /// Konwerter bool do Visibility
    /// </summary>
    public class Bool2VisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (Visibility)value == Visibility.Visible;
        }
    }
}
