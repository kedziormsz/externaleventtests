﻿using System;
using System.Collections.Generic;
using System.Threading;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;

namespace ExternalEventTests
{
    class FlipFacingHandler : IExternalEventHandler
    {
        ViewModel vm;

        public FlipFacingHandler(ViewModel vm)
        {
            this.vm = vm;
        }

        public void Execute(UIApplication app)
        {
            var doc = app.ActiveUIDocument.Document;

            var doorset = vm.doors ?? new FilteredElementCollector(doc, doc.ActiveView.Id)
                .OfCategory(BuiltInCategory.OST_Doors)
                .ToElements();

            using (Transaction tx = new Transaction(doc))
            {
                tx.Start("FlipFacingHandlerExecute");

                for (int i = 0; i < doorset.Count; i++)
                {
                    vm.setStatus(GetName() + i);

                    (doorset[i] as FamilyInstance).flipFacing();

                    Thread.Sleep(50); // dummy slowdown only
                }

                tx.Commit();
            }
        }

        public string GetName()
        {
            return "FlipFacingHandler";
        }
    }
}
