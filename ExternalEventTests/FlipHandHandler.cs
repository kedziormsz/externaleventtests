﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Threading;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;

namespace ExternalEventTests
{
    class FlipHandHandler : IExternalEventHandler
    {
        ViewModel vm;
        Action<object> setStatus;

        public FlipHandHandler(ViewModel vm, Action<object> setStatus)
        {
            this.vm = vm;
            this.setStatus = setStatus;
        }

        public void Execute(UIApplication app)
        {
            var doc = app.ActiveUIDocument.Document;

            var doorset = vm.doors ?? new FilteredElementCollector(doc)
                .OfCategory(BuiltInCategory.OST_Doors)
                .ToElements();

            for (int i = 0; i < doorset.Count; i++)
            {
                setStatus(GetName() + i);

                using (Transaction tx = new Transaction(doc)) // dummy slowdown only
                {
                    tx.Start("FlipHandHandlerExecute" + i);
                    try
                    {
                        (doorset[i] as FamilyInstance).flipHand();
                    }
                    catch (Exception e)
                    {
                        //Debug.Assert(false, e.ToString());
                        //vm.Status = e.Message; 
                    }
                    tx.Commit();
                }
            }
        }

        public string GetName()
        {
            return "FlipHandHandler";
        }
    }
}
