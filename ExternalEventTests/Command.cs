#region Namespaces
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Interop;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using BuildingCoder;
#endregion

namespace ExternalEventTests
{
    [Transaction(TransactionMode.Manual)]
    public class WpfCommand : IExternalCommand
    {
        public Result Execute(
          ExternalCommandData commandData,
          ref string message,
          ElementSet elements)
        {
            // nie dziala
            //AppDomain.CurrentDomain.UnhandledException += (s, e) => { Debug.Assert(false, e.ToString()); };

            var mw = new WpfWindow();
            new WindowInteropHelper(mw) { Owner = System.Diagnostics.Process.GetCurrentProcess().MainWindowHandle };
            mw.DataContext = new ViewModel(commandData, () => mw.DoEvents());
            mw.Show();

            return Result.Succeeded;
        }
    }

    [Transaction(TransactionMode.Manual)]
    public class FormsCommand : IExternalCommand
    {
        public Result Execute(
          ExternalCommandData commandData,
          ref string message,
          ElementSet elements)
        {
            new FormsWindow(new ViewModel(commandData, null).initExEvents())
                .Show(new JtWindowHandle(System.Diagnostics.Process.GetCurrentProcess().MainWindowHandle));

            return Result.Succeeded;
        }
    }

    [Transaction(TransactionMode.Manual)]
    public class Command : IExternalCommand
    {
        public Result Execute(
          ExternalCommandData commandData,
          ref string message,
          ElementSet elements)
        {
            UIApplication uiapp = commandData.Application;
            UIDocument uidoc = uiapp.ActiveUIDocument;
            Application app = uiapp.Application;
            Document doc = uidoc.Document;

            // Access current selection

            Selection sel = uidoc.Selection;

            // Retrieve elements from database

            FilteredElementCollector col
              = new FilteredElementCollector(doc)
                .WhereElementIsNotElementType()
                .OfCategory(BuiltInCategory.INVALID)
                .OfClass(typeof(Wall));

            // Filtered element collector is iterable

            foreach (Element e in col)
            {
                Debug.Print(e.Name);
            }

            // Modify document within a transaction

            using (Transaction tx = new Transaction(doc))
            {
                tx.Start("Transaction Name");
                tx.Commit();
            }

            return Result.Succeeded;
        }
    }
}
